/**
 * @brief Simple C++14 profiler
 * @author Wolfram Rösler <wolfram@roesler-ac.de>
 * @date 2018-07-07
 */

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <math.h>
#include <mutex>
#include <string>
#include <regex>
#include <vector>
#include <unordered_map>
#include "profiler.h"

namespace {
    // The output file into which we write the profiler results
    const auto file = "./profiler.out";

    // Output resolution: false=milliseconds, true=microseconds
    auto constexpr microseconds = false;

    /*
     * Data type for one call point.
     */
    struct Item {
        std::string name;                       // Name of the call point
        unsigned int nCalls = 0;                // Number of calls
        unsigned long usTotal = 0;              // Due to a gcc bug we cannot name this µsTotal
        unsigned long usMin = std::numeric_limits<unsigned long>::max();
        unsigned long usMax = 0;                // Maximum duration
        double mean = 0.0;                      // Mean value
        double m2 = 0.0;                        // Sum of squares of differences from mean
    };

    /*
     * Profile data collector.
     */
    class Collector {
    public:
        ~Collector();
        void Add(const profiler::Profiler&);

    private:
        std::unordered_map<std::string,Item> items_;
        std::mutex mtx_;
    } collector;

    // Measure the program's total duration.
    // Do this after the "collector" object defined above, so the profiler
    // object is constructed after and destroyed before the collector.
    PROFILE("Total");
}

/*
 * Profile data collector.
 * Adds the duration since construction of prf to the profile data.
 */
void Collector::Add(const profiler::Profiler& prf) {

    // Don't crash if some joker passed a null pointer
    if (!prf.name) return;

    // Compute the duration
    const unsigned long us = std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::high_resolution_clock::now() - prf.start
    ).count();

    // Lock access to the items map
    std::lock_guard<std::mutex> lock(mtx_);

    // Find the item to store the data
    auto& item = items_[prf.name];

    // Store it
    item.nCalls++;
    item.usTotal += us;
    item.usMin = std::min(item.usMin,us);
    item.usMax = std::max(item.usMax,us);

    // Welford algorith for online variance computation
    // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm
    const auto delta = us - item.mean;
    item.mean += delta / item.nCalls;
    const auto delta2 = us - item.mean;
    item.m2 += delta* delta2;

    // Store the name if not yet done
    if (item.name.empty()) {
        item.name = prf.name;
    }
}

/*
 * Dtor of profile data collector.
 * Writes profile data to the output file.
 */
Collector::~Collector() {

    // Create the output file
    auto ofs = std::ofstream(file);
    if (!ofs) {
        std::cerr << "Profiler: Cannot create output file " << file << std::endl;
        return;
    }

    // Sort the data by duration (decending)
    std::vector<const Item*> vec;
    vec.reserve(items_.size());
    for(auto& p : items_) {
        vec.push_back(&p.second);
    }
    std::sort(vec.begin(),vec.end(),[](const Item* a,const Item* b) { return a->usTotal > b->usTotal; });

    // Output the table header
    auto constexpr w = microseconds ? 15 : 10;
    const auto fw = std::setw(w);
    ofs << fw << "# calls"
        << fw << "Total"
        << fw << "Min"
        << fw << "Max"
        << fw << "Avg"
        << fw << "StdDev"
        << " | Call point"
        << "\n"
        << std::string(6*w,'-') << "-+-" << std::string(20,'-') << "\n";

    // Output the collected data
    for(const auto& i : vec) {
        auto show = [](unsigned long us) { return microseconds ? us : us / 1000; };
        ofs << fw << i->nCalls
            << fw << show(i->usTotal)
            << fw << show(i->usMin)
            << fw << show(i->usMax)
            << fw << show(i->usTotal / i->nCalls)
            << fw << show(i->nCalls < 2 ? 0 : sqrt(i->m2 / i->nCalls))
            << " | " << i->name
            << "\n";
    }

    // We don't want to repeat the unit in the table header for each column
    ofs << "\nAll durations in "
        << (microseconds ? "microseconds" : "milliseconds")
        << "\n";
}

namespace profiler {
    /*
     * Dtor of profiler class.
     * Adds the time since ctor to the profile data collector.
     */
    Profiler::~Profiler() {
        collector.Add(*this);
    }
}
