# Simple Profiling With C++14

This is a simple profiling tool to quickly monitor the performance of a block (function, loop, etc.) of your C++ program:

* How many times was it executed
* How long did it take to execute in total
* Which is the minimum, maximum, and average execution time

It's a C++ only solution that doesn't require external tools. Just add `profiler.cpp` to your project (or to a library) and include `profiler.h` in your source file. Then, at the beginning of every block you want to monitor, add the following line:

```cpp
PROFILE("name of block");
```

The "name of block" is a string that uniquely identifies the block. Usually, that line is at the beginning of a function, and the "name of block" is the function name. For example:

```cpp
void MySlowFunction() {
    PROFILE("MySlowFunction");

    for(...) {
        DoSomething();
    }
}
```

With these `PROFILE` lines in place, build and run your program. When the program exits, it creates a file named `profiler.out` in the current directory which contains the following information about all `PROFILE` blocks:

* Number of calls: The number of calls the block was executed.
* Total: The total duration of all executions of the block.
* Min: The minimum (fastest) duration of an execution of the block.
* Max: The maximum (slowest) duration of an execution of the block.
* Avg: The average duration (total duration / number of calls).
* StdDev: The standard deviation of the execution times of the block.
* Call point: Your argument to `PROFILE`.

Durations can be in milliseconds or in microseconds, which you define in `profile.cpp`:

```cpp
// Output resolution: false=milliseconds, true=microseconds
auto constexpr microseconds = false;
```

Real-life examples output:

```
   # calls     Total       Min       Max       Avg    StdDev | Call point
-------------------------------------------------------------+---------------------
         2     13800      6900      6900      6900         0 | StartWebserver
         1      6923      6923      6923      6923         0 | Total
        58      2396        10      1016        41       132 | HandleRequest
        58      1800         0      1006        31       132 | HandleURI
       116      1170        10        10        10         0 | IsAuthorized
       174         0         0         0         0         0 | Authorization
        52         0         0         0         0         0 | RequestBody

All durations in milliseconds
```

## Running The Example Program

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./profiling
$ cat profiler.out
```

## Credits

The original idea for this class was taken from the QProfile toolkit on CodeGuru: https://www.codeguru.com/cpp/v-s/debug/article.php/c1253/QProfile-toolkit--a-class-to-profile-your-code.htm

Over the years, I wrote and re-wrote my own implementation of the profiler class several times. The implementation in this repository doesn't contain any code from CodeGuru's QProfile.

---
*Wolfram Rösler • wolfram@roesler-ac.de • https://gitlab.com/wolframroesler • https://mastodontech.de/@wolfram_roesler • https://www.linkedin.com/in/wolframroesler/*
